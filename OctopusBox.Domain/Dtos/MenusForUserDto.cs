using System;
using System.Collections;
using Newtonsoft.Json;
using OctopusBox.Domain.Models;

namespace OctopusBox.Domain.Dtos
{
    public class MenusForUserDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("parentid")]
        public int? ParentId { get; set; }

        [JsonProperty("menuname")]
        public string MenuName { get; set; }

        [JsonProperty("showinmenu")]
        public BitArray ShowInMenu { get; set; }

        [JsonProperty("sortindex")]
        public int? SortIndex { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("iconstring")]
        public string IconString { get; set; }
        
       // public Menus[] SubMenus { get; set; }
    }
}