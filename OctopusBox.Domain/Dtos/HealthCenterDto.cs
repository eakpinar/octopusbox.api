using System;
using Newtonsoft.Json;

namespace OctopusBox.Domain.Dtos
{
    public class HealthCenterDto
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("cgmpassword")]
        public string CgmPassword { get; set; }
        [JsonProperty("healthcentergroupid")]
        public long? HealthCenterGroupId { get; set; }
    }
}