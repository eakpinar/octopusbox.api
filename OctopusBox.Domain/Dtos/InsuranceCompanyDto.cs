using System;
using Newtonsoft.Json;

namespace OctopusBox.Domain.Dtos
{
    public class InsuranceCompanyDto
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}