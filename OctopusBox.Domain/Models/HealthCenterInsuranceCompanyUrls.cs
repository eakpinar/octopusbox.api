﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class HealthCenterInsuranceCompanyUrls : ModelBase
    {
        public long Id { get; set; }
        public long? HealthCenterId { get; set; }
        public long? InsuranceCompanyConnectionId { get; set; }
        public string HealthCenterName { get; set; }
        public string HealthCenterPassword { get; set; }

        public virtual HealthCenter HealthCenter { get; set; }
        public virtual InsuranceCompanyConnection InsuranceCompanyConnection { get; set; }
    }
}
