﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class Environment : ModelBase
    {
        public Environment()
        {
            InsuranceCompanyConnection = new HashSet<InsuranceCompanyConnection>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<InsuranceCompanyConnection> InsuranceCompanyConnection { get; set; }
    }
}
