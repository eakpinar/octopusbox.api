using System;
using System.Collections.Generic;
using System.Collections;

namespace OctopusBox.Domain.Models
{
    public partial class Menus : ModelBase
    {
        public Menus()
        {
            SubMenus = new HashSet<Menus>();
            RoleMenuRel = new HashSet<RoleMenuRel>();
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string MenuName { get; set; }
        public BitArray ShowInMenu { get; set; }
        public int? SortIndex { get; set; }
        public string Link { get; set; }
        public string IconString { get; set; }
        public ICollection<Menus> SubMenus { get; set; }
        public virtual ICollection<RoleMenuRel> RoleMenuRel { get; set; }
    }
}