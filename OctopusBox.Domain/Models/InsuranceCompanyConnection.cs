﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class InsuranceCompanyConnection : ModelBase
    {
        public InsuranceCompanyConnection()
        {
            HealthCenterInsuranceCompanyUrls = new HashSet<HealthCenterInsuranceCompanyUrls>();
        }

        public long Id { get; set; }
        public long? InsuranceCompanyId { get; set; }
        public string ConnectionUrl { get; set; }
        public long? ModuleId { get; set; }
        public long? EnvironmentId { get; set; }

        public virtual Environment Environment { get; set; }
        public virtual Module Module { get; set; }
        public virtual ICollection<HealthCenterInsuranceCompanyUrls> HealthCenterInsuranceCompanyUrls { get; set; }
    }
}
