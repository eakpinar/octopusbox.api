﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class InsuranceCompanyFunction : ModelBase
    {
        public long Id { get; set; }
        public long? InsuranceCompanyId { get; set; }
        public long? FunctionId { get; set; }

        public virtual Function Function { get; set; }
        public virtual InsuranceCompany InsuranceCompany { get; set; }
    }
}
