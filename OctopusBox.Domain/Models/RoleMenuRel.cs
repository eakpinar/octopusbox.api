using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class RoleMenuRel: ModelBase
    {
        public int Id { get; set; }
        public int? MenuId { get; set; }
        public long? RoleId { get; set; }
        public virtual Menus Menu { get; set; }
        public virtual Roles Role { get; set; }
    }
}