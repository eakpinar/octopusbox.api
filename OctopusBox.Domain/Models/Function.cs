﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class Function : ModelBase
    {
        public Function()
        {
            HealthCenterInsuranceCompanyFuncs = new HashSet<HealthCenterInsuranceCompanyFuncs>();
            InsuranceCompanyFunction = new HashSet<InsuranceCompanyFunction>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public long? ModuleId { get; set; }

        public virtual Module Module { get; set; }
        public virtual ICollection<HealthCenterInsuranceCompanyFuncs> HealthCenterInsuranceCompanyFuncs { get; set; }
        public virtual ICollection<InsuranceCompanyFunction> InsuranceCompanyFunction { get; set; }
    }
}
