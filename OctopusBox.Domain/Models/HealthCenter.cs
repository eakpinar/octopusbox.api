﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class HealthCenter : ModelBase
    {

        public HealthCenter(long id, string name, string cgmPassword, long healthCenterGroupId)
        {
            Id = id;
            Name = name;
            CgmPassword = cgmPassword;
            HealthCenterGroupId = healthCenterGroupId;
        }
        public HealthCenter()
        {
            HealthCenterInsuranceCompanyFuncs = new HashSet<HealthCenterInsuranceCompanyFuncs>();
            HealthCenterInsuranceCompanyUrls = new HashSet<HealthCenterInsuranceCompanyUrls>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string CgmPassword { get; set; }
        public long? HealthCenterGroupId { get; set; }

        public virtual HealthCenterGroup HealthCenterGroup { get; set; }
        public virtual ICollection<HealthCenterInsuranceCompanyFuncs> HealthCenterInsuranceCompanyFuncs { get; set; }
        public virtual ICollection<HealthCenterInsuranceCompanyUrls> HealthCenterInsuranceCompanyUrls { get; set; }
    }
}
