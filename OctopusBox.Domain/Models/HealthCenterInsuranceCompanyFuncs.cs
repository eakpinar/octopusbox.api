﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class HealthCenterInsuranceCompanyFuncs : ModelBase
    {
        public long Id { get; set; }
        public long? HealthCenterId { get; set; }
        public long? InsuranceCompanyId { get; set; }
        public long? FunctionId { get; set; }

        public virtual Function Function { get; set; }
        public virtual HealthCenter HealthCenter { get; set; }
        public virtual InsuranceCompany InsuranceCompany { get; set; }
    }
}
