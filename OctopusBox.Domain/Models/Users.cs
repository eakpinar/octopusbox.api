﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class Users : ModelBase
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public long? RoleId { get; set; }
        public virtual Roles Role { get; set; }
    }
}
