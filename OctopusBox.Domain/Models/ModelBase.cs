using System;

namespace OctopusBox.Domain.Models
{
    public class ModelBase
    {
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}