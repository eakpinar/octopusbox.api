﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class Module : ModelBase
    {
        public Module()
        {
            Function = new HashSet<Function>();
            InsuranceCompanyConnection = new HashSet<InsuranceCompanyConnection>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Function> Function { get; set; }
        public virtual ICollection<InsuranceCompanyConnection> InsuranceCompanyConnection { get; set; }
    }
}
