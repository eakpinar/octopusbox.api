﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class HealthCenterGroup : ModelBase
    {
        public HealthCenterGroup()
        {
            HealthCenter = new HashSet<HealthCenter>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<HealthCenter> HealthCenter { get; set; }
    }
}
