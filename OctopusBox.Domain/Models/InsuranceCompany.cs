﻿using System;
using System.Collections.Generic;

namespace OctopusBox.Domain.Models
{
    public partial class InsuranceCompany : ModelBase
    {
        /// <summary>
        /// New record
        ///</summary>
        public InsuranceCompany(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public InsuranceCompany()
        {
            HealthCenterInsuranceCompanyFuncs = new HashSet<HealthCenterInsuranceCompanyFuncs>();
            InsuranceCompanyFunction = new HashSet<InsuranceCompanyFunction>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<HealthCenterInsuranceCompanyFuncs> HealthCenterInsuranceCompanyFuncs { get; set; }
        public virtual ICollection<InsuranceCompanyFunction> InsuranceCompanyFunction { get; set; }
    }
}
