using System;
using System.Collections.Generic;
using OctopusBox.Domain.Models;

namespace OctopusBox.Domain.Models
{
    public partial class Roles: ModelBase
    {
        public Roles()
        {
            RoleMenuRel = new HashSet<RoleMenuRel>();
            Users = new HashSet<Users>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<RoleMenuRel> RoleMenuRel { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}