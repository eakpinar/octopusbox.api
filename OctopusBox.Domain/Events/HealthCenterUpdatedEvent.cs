using System;
using MediatR;

namespace OctopusBox.Domain.Events
{
    public class HealthCenterUpdatedEvent : INotification
    {
        public long HealthCenterId { get; }

        public HealthCenterUpdatedEvent(long healthCenterId)
        {
            HealthCenterId = healthCenterId;
        }
    }
}