using System;
using MediatR;

namespace OctopusBox.Domain.Events
{
    public class InsuranceCompanyDeletedEvent : INotification
    {
        public long InsuranceCompanyId { get; }

        public InsuranceCompanyDeletedEvent(long insuranceCompanyId)
        {
            InsuranceCompanyId = insuranceCompanyId;
        }
    }
}