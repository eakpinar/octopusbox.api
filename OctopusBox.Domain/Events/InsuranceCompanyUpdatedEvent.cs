using System;
using MediatR;

namespace OctopusBox.Domain.Events
{
    public class InsuranceCompanyUpdatedEvent : INotification
    {
        public long InsuranceCompanyId { get; }

        public InsuranceCompanyUpdatedEvent(long insuranceCompanyId)
        {
            InsuranceCompanyId = insuranceCompanyId;
        }
    }
}