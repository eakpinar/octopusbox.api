using System;
using MediatR;

namespace OctopusBox.Domain.Events
{
    public class HealthCenterDeletedEvent : INotification
    {
        public long HealthCenterId { get; }

        public HealthCenterDeletedEvent(long healthCenterId)
        {
            HealthCenterId = healthCenterId;
        }
    }
}