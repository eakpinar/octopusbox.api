using System;
using MediatR;

namespace OctopusBox.Domain.Events
{
    public class HealthCenterCreatedEvent : INotification
    {
        public long HealthCenterId { get; }

        public HealthCenterCreatedEvent(long healthCenterId)
        {
            HealthCenterId = healthCenterId;
        }
    }
}