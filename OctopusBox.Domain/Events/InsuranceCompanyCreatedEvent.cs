using System;
using MediatR;

namespace OctopusBox.Domain.Events
{
    public class InsuranceCompanyCreatedEvent : INotification
    {
        public long InsuranceCompanyId { get; }

        public InsuranceCompanyCreatedEvent(long insuranceCompanyId)
        {
            InsuranceCompanyId = insuranceCompanyId;
        }
    }
}