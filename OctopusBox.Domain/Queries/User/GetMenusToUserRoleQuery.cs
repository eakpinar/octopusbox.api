using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Domain.Queries.User
{
    public class GetMenusToUserRoleQuery : QueryBase<MenusForUserDto>
    {
        public GetMenusToUserRoleQuery()
        {
        }

        [JsonConstructor]
        public GetMenusToUserRoleQuery(long userId)
        {
            UserId = userId;
        }

        [JsonProperty("id")]
        [Required]
        public long UserId { get; set; }
    }
}