using System.Collections.Generic;
using Newtonsoft.Json;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Domain.Queries.InsuranceCompany
{
    public class GetAllInsuranceCompaniesQuery : QueryBase<List<InsuranceCompanyDto>>
    {
        [JsonConstructor]
        public GetAllInsuranceCompaniesQuery()
        {
        }
    }
}