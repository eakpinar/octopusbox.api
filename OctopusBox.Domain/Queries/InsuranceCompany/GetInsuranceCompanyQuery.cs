using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Domain.Queries.InsuranceCompany
{
    public class GetInsuranceCompanyQuery : QueryBase<InsuranceCompanyDto>
    {
        public GetInsuranceCompanyQuery()
        {
        }

        [JsonConstructor]
        public GetInsuranceCompanyQuery(long insuranceCompanyId)
        {
            InsuranceCompanyId = insuranceCompanyId;
        }

        [JsonProperty("id")]
        [Required]
        public long InsuranceCompanyId { get; set; }
    }
}