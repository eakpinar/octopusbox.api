using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Domain.Queries.HealthCenter
{
    public class GetHealthCenterQuery : QueryBase<HealthCenterDto>
    {
        public GetHealthCenterQuery()
        {
        }

        [JsonConstructor]
        public GetHealthCenterQuery(long healthCenterId)
        {
            HealthCenterId = healthCenterId;
        }

        [JsonProperty("id")]
        [Required]
        public long HealthCenterId { get; set; }
    }
}