using System.Collections.Generic;
using Newtonsoft.Json;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Domain.Queries.HealthCenter
{
    public class GetAllHealthCentersQuery : QueryBase<List<HealthCenterDto>>
    {
        [JsonConstructor]
        public GetAllHealthCentersQuery()
        {
        }
    }
}