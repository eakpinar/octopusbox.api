using MediatR;
using System.Collections.Generic;

namespace OctopusBox.Domain.Queries
{
    public abstract class QueryBase<TResult> : IRequest<TResult> where TResult : class
    {
        
    }
}