using System.ComponentModel.DataAnnotations;
using OctopusBox.Domain.Dtos;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace OctopusBox.Domain.Commands
{
    public class DeleteInsuranceCompanyCommand : CommandBase<InsuranceCompanyDto>
    {
        public DeleteInsuranceCompanyCommand()
        {
        }

        [JsonConstructor]
        public DeleteInsuranceCompanyCommand(long id)
        {
            Id = id;
        }
        
        [JsonProperty("id")]
        [Required]
        public long Id { get; }

    }
}