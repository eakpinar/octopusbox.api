using System.ComponentModel.DataAnnotations;
using OctopusBox.Domain.Dtos;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace OctopusBox.Domain.Commands
{
    public class CreateInsuranceCompanyCommand : CommandBase<InsuranceCompanyDto>
    {
        public CreateInsuranceCompanyCommand()
        {
        }

        [JsonConstructor]
        public CreateInsuranceCompanyCommand(long id, string name)
        {
            Id = id; 
            Name = name;
        }
        
        [JsonProperty("id")]
        [Required]
        public long Id { get; }

        [JsonProperty("name")]
        [Required]
        [MaxLength(255)]
        public string Name { get; }

    }
}