using MediatR;

namespace OctopusBox.Domain.Commands
{
    public class CommandBase<T> : IRequest<T> where T : class
    {
        
    }
}