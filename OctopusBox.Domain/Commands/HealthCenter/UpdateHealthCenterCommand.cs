using System.ComponentModel.DataAnnotations;
using OctopusBox.Domain.Dtos;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace OctopusBox.Domain.Commands
{
    public class UpdateHealthCenterCommand : CommandBase<HealthCenterDto>
    {
        public UpdateHealthCenterCommand()
        {
        }

        [JsonConstructor]
        public UpdateHealthCenterCommand(long id, string name, string cgmPassword, long healthCenterGroupId)
        {
            Id = id; 
            Name = name;
            CgmPassword = cgmPassword;
            HealthCenterGroupId = healthCenterGroupId;
        }
        
        [JsonProperty("id")]
        [Required]
        public long Id { get; }

        [JsonProperty("name")]
        [Required]
        [MaxLength(255)]
        public string Name { get; }

        [JsonProperty("cgmpassword")]
        [Required]
        public string CgmPassword { get; }

        [JsonProperty("healthcentergroupid")]
        [Required]
        public long HealthCenterGroupId { get; }
    }
}