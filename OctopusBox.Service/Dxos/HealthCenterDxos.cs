using AutoMapper;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Service.Dxos
{
    public class HealthCenterDxos : IHealthCenterDxos
    {
        private readonly IMapper _mapper;

        public HealthCenterDxos()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Models.HealthCenter, Domain.Dtos.HealthCenterDto>()
                    .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dst => dst.CgmPassword, opt => opt.MapFrom(src => src.CgmPassword))
                    .ForMember(dst => dst.HealthCenterGroupId, opt => opt.MapFrom(src => src.HealthCenterGroupId));
            });

            _mapper = config.CreateMapper();
        }
        
        public HealthCenterDto MapHealthCenterDto(Domain.Models.HealthCenter healthCenter)
        {
            return _mapper.Map<Domain.Models.HealthCenter, Domain.Dtos.HealthCenterDto>(healthCenter);
        }
    }
}