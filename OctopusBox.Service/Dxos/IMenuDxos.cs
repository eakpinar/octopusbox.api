using OctopusBox.Domain.Dtos;

namespace OctopusBox.Service.Dxos
{
    public interface IMenuDxos
    {
        MenuDto MapMenuDto(Domain.Models.Menus Menu);
    }
}