using AutoMapper;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Service.Dxos
{
    public class InsuranceCompanyDxos : IInsuranceCompanyDxos
    {
        private readonly IMapper _mapper;

        public InsuranceCompanyDxos()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Models.InsuranceCompany, Domain.Dtos.InsuranceCompanyDto>()
                    .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));
            });

            _mapper = config.CreateMapper();
        }
        
        public InsuranceCompanyDto MapInsuranceCompanyDto(Domain.Models.InsuranceCompany insuranceCompany)
        {
            return _mapper.Map<Domain.Models.InsuranceCompany, Domain.Dtos.InsuranceCompanyDto>(insuranceCompany);
        }
    }
}