using OctopusBox.Domain.Dtos;

namespace OctopusBox.Service.Dxos
{
    public interface IHealthCenterDxos
    {
        HealthCenterDto MapHealthCenterDto(Domain.Models.HealthCenter HealthCenter);
    }
}