using AutoMapper;
using OctopusBox.Domain.Dtos;

namespace OctopusBox.Service.Dxos
{
    public class MenuDxos : IMenuDxos
    {
        private readonly IMapper _mapper;

        public MenuDxos()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Models.Menus, Domain.Dtos.MenuDto>()
                    .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dst => dst.ParentId, opt => opt.MapFrom(src => src.ParentId))
                    .ForMember(dst => dst.MenuName, opt => opt.MapFrom(src => src.MenuName))
                    .ForMember(dst => dst.ShowInMenu, opt => opt.MapFrom(src => src.ShowInMenu))
                    .ForMember(dst => dst.SortIndex, opt => opt.MapFrom(src => src.SortIndex))
                    .ForMember(dst => dst.Link, opt => opt.MapFrom(src => src.Link))
                    .ForMember(dst => dst.IconString, opt => opt.MapFrom(src => src.IconString));
            });

            _mapper = config.CreateMapper();
        }
        
        public MenuDto MapMenuDto(Domain.Models.Menus Menu)
        {
            return _mapper.Map<Domain.Models.Menus, Domain.Dtos.MenuDto>(Menu);
        }
    }
}