using OctopusBox.Domain.Dtos;

namespace OctopusBox.Service.Dxos
{
    public interface IInsuranceCompanyDxos
    {
        InsuranceCompanyDto MapInsuranceCompanyDto(Domain.Models.InsuranceCompany InsuranceCompany);
    }
}