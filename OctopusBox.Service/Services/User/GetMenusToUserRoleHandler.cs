using System;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using OctopusBox.Service.Dxos;
using MediatR;
using Microsoft.Extensions.Logging;
using OctopusBox.Domain.Queries.InsuranceCompany;
using OctopusBox.Domain.Queries.User;

namespace OctopusBox.Service.Services
{
    public class GetMenusToUserRoleHandler : IRequestHandler<GetMenusToUserRoleQuery, MenusForUserDto>
    {
        private readonly IRepository<Domain.Models.Menus> _menusRepository;
        private readonly IMenuDxos _menuDxos;
        private readonly ILogger _logger;
        
        public GetMenusToUserRoleHandler(IRepository<Domain.Models.Menus> menusRepository, IMenuDxos menuDxos, ILogger<GetMenusToUserRoleHandler> logger)
        {
            _menusRepository = menusRepository ?? throw new ArgumentException(nameof(menusRepository));
            _menuDxos = menuDxos ?? throw new ArgumentException(nameof(menuDxos));
            _logger = logger ?? throw new ArgumentException(nameof(logger));
        }

        public Task<MenusForUserDto> Handle(GetMenusToUserRoleQuery request, CancellationToken cancellationToken)
        {
            /*
            var result = await _insuranceCompanyRepository.GetAsync(e =>
                e.Id == request.InsuranceCompanyId);

            if (result != null)
            {
                _logger.LogInformation($"Got a request get insuranceCompany Id: {result.Id}");
                var insuranceCompanyDto = _insuranceCompanyDxos.MapInsuranceCompanyDto(result);
                return insuranceCompanyDto;
            }*/

            return null;
            throw new NotImplementedException();
        }
    }
}