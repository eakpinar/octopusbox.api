using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using OctopusBox.Domain.Models;
using OctopusBox.Service.Dxos;
using MediatR;
using Microsoft.Extensions.Logging;
using OctopusBox.Domain.Queries.InsuranceCompany;

namespace OctopusBox.Service.Services
{
    public class GetAllInsuranceCompaniesHandler : IRequestHandler<GetAllInsuranceCompaniesQuery, List<InsuranceCompanyDto>>
    {
        private readonly IInsuranceCompanyRepository _insuranceCompanyRepository;
        private readonly IInsuranceCompanyDxos _insuranceCompanyDxos;
        private readonly ILogger _logger;

        public GetAllInsuranceCompaniesHandler(IInsuranceCompanyRepository insuranceCompanyRepository, IInsuranceCompanyDxos insuranceCompanyDxos, ILogger<GetInsuranceCompanyHandler> logger)
        {
            _insuranceCompanyRepository = insuranceCompanyRepository ?? throw new ArgumentNullException(nameof(insuranceCompanyRepository));
            _insuranceCompanyDxos = insuranceCompanyDxos ?? throw new ArgumentNullException(nameof(insuranceCompanyDxos));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<List<InsuranceCompanyDto>> Handle(GetAllInsuranceCompaniesQuery request, CancellationToken cancellationToken)
        {
            var result = await _insuranceCompanyRepository.GetListAsync(e => e.Id != 0);

            if (result != null)
            {
                _logger.LogInformation("Got a request get all insurance companies.");
                List<InsuranceCompanyDto> insuranceCompanyDtos = new List<InsuranceCompanyDto>();
                
                foreach (var item in result)
                {
                    InsuranceCompanyDto insuranceCompanyDto = _insuranceCompanyDxos.MapInsuranceCompanyDto(item);
                    insuranceCompanyDtos.Add(insuranceCompanyDto);
                }
                return insuranceCompanyDtos;
            }

            return null;
        }
    }
}