using System;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Commands;
using OctopusBox.Domain.Dtos;
using OctopusBox.Service.Dxos;
using MediatR;

namespace OctopusBox.Service.Services
{
    public class DeleteInsuranceCompanyHandler : IRequestHandler<DeleteInsuranceCompanyCommand, InsuranceCompanyDto>
    {
        private readonly IInsuranceCompanyRepository _insuranceCompanyRepository;
        private readonly IInsuranceCompanyDxos _insuranceCompanyDxos;
        private readonly IMediator _mediator;

        public DeleteInsuranceCompanyHandler(IInsuranceCompanyRepository insuranceCompanyRepository, IMediator mediator, IInsuranceCompanyDxos insuranceCompanyDxos)
        {
            _insuranceCompanyRepository = insuranceCompanyRepository ?? throw new ArgumentNullException(nameof(insuranceCompanyRepository));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _insuranceCompanyDxos = insuranceCompanyDxos ?? throw new ArgumentNullException(nameof(insuranceCompanyDxos));
        }

        public async  Task<InsuranceCompanyDto> Handle(DeleteInsuranceCompanyCommand request, CancellationToken cancellationToken)
        {
            var insuranceCompany = new Domain.Models.InsuranceCompany(request.Id, null);
            
            _insuranceCompanyRepository.Remove(insuranceCompany);

            if (await _insuranceCompanyRepository.SaveChangesAsync() == 0)
            {
                throw new ApplicationException();
            }

            await _mediator.Publish(new Domain.Events.InsuranceCompanyDeletedEvent(insuranceCompany.Id), cancellationToken);

            var insuranceCompanyDto = _insuranceCompanyDxos.MapInsuranceCompanyDto(insuranceCompany);
            return insuranceCompanyDto;
        }
    }
}