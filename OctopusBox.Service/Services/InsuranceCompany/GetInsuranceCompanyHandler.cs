using System;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using OctopusBox.Service.Dxos;
using MediatR;
using Microsoft.Extensions.Logging;
using OctopusBox.Domain.Queries.InsuranceCompany;

namespace OctopusBox.Service.Services
{
    public class GetInsuranceCompanyHandler : IRequestHandler<GetInsuranceCompanyQuery, InsuranceCompanyDto>
    {
        private readonly IInsuranceCompanyRepository _insuranceCompanyRepository;
        private readonly IInsuranceCompanyDxos _insuranceCompanyDxos;
        private readonly ILogger _logger;

        public GetInsuranceCompanyHandler(IInsuranceCompanyRepository insuranceCompanyRepository, IInsuranceCompanyDxos insuranceCompanyDxos, ILogger<GetInsuranceCompanyHandler> logger)
        {
            _insuranceCompanyRepository = insuranceCompanyRepository ?? throw new ArgumentNullException(nameof(insuranceCompanyRepository));
            _insuranceCompanyDxos = insuranceCompanyDxos ?? throw new ArgumentNullException(nameof(insuranceCompanyDxos));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<InsuranceCompanyDto> Handle(GetInsuranceCompanyQuery request, CancellationToken cancellationToken)
        {
            var result = await _insuranceCompanyRepository.GetAsync(e =>
                e.Id == request.InsuranceCompanyId);

            if (result != null)
            {
                _logger.LogInformation($"Got a request get insuranceCompany Id: {result.Id}");
                var insuranceCompanyDto = _insuranceCompanyDxos.MapInsuranceCompanyDto(result);
                return insuranceCompanyDto;
            }

            return null;
        }
    }
}