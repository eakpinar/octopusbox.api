using System;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using OctopusBox.Service.Dxos;
using MediatR;
using Microsoft.Extensions.Logging;
using OctopusBox.Domain.Queries.HealthCenter;

namespace OctopusBox.Service.Services
{
    public class GetHealthCenterHandler : IRequestHandler<GetHealthCenterQuery, HealthCenterDto>
    {
        private readonly IHealthCenterRepository _healthCenterRepository;
        private readonly IHealthCenterDxos _healthCenterDxos;
        private readonly ILogger _logger;

        public GetHealthCenterHandler(IHealthCenterRepository healthCenterRepository, IHealthCenterDxos healthCenterDxos, ILogger<GetHealthCenterHandler> logger)
        {
            _healthCenterRepository = healthCenterRepository ?? throw new ArgumentNullException(nameof(healthCenterRepository));
            _healthCenterDxos = healthCenterDxos ?? throw new ArgumentNullException(nameof(healthCenterDxos));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<HealthCenterDto> Handle(GetHealthCenterQuery request, CancellationToken cancellationToken)
        {
            var result = await _healthCenterRepository.GetAsync(e =>
                e.Id == request.HealthCenterId);

            if (result != null)
            {
                _logger.LogInformation($"Got a request get healthCenter Id: {result.Id}");
                var healthCenterDto = _healthCenterDxos.MapHealthCenterDto(result);
                return healthCenterDto;
            }

            return null;
        }
    }
}