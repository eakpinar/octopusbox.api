using System;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Commands;
using OctopusBox.Domain.Dtos;
using OctopusBox.Service.Dxos;
using MediatR;

namespace OctopusBox.Service.Services
{
    public class CreateHealthCenterHandler : IRequestHandler<CreateHealthCenterCommand, HealthCenterDto>
    {
        private readonly IHealthCenterRepository _healthCenterRepository;
        private readonly IHealthCenterDxos _healthCenterDxos;
        private readonly IMediator _mediator;

        public CreateHealthCenterHandler(IHealthCenterRepository healthCenterRepository, IMediator mediator, IHealthCenterDxos healthCenterDxos)
        {
            _healthCenterRepository = healthCenterRepository ?? throw new ArgumentNullException(nameof(healthCenterRepository));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _healthCenterDxos = healthCenterDxos ?? throw new ArgumentNullException(nameof(healthCenterDxos));
        }


        public async  Task<HealthCenterDto> Handle(CreateHealthCenterCommand request, CancellationToken cancellationToken)
        {
            var healthCenter = new Domain.Models.HealthCenter(request.Id, request.Name, request.CgmPassword, request.HealthCenterGroupId);
            
            _healthCenterRepository.Add(healthCenter);

            if (await _healthCenterRepository.SaveChangesAsync() == 0)
            {
                throw new ApplicationException();
            }

            await _mediator.Publish(new Domain.Events.HealthCenterCreatedEvent(healthCenter.Id), cancellationToken);

            var healthCenterDto = _healthCenterDxos.MapHealthCenterDto(healthCenter);
            return healthCenterDto;
        }
    }
}