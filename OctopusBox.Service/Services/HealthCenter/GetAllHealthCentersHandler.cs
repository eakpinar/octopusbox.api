using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using OctopusBox.Domain.Models;
using OctopusBox.Service.Dxos;
using MediatR;
using Microsoft.Extensions.Logging;
using OctopusBox.Domain.Queries.HealthCenter;

namespace OctopusBox.Service.Services
{
    public class GetAllHealthCentersHandler : IRequestHandler<GetAllHealthCentersQuery, List<HealthCenterDto>>
    {
        private readonly IHealthCenterRepository _healthCenterRepository;
        private readonly IHealthCenterDxos _healthCenterDxos;
        private readonly ILogger _logger;

        public GetAllHealthCentersHandler(IHealthCenterRepository healthCenterRepository, IHealthCenterDxos healthCenterDxos, ILogger<GetHealthCenterHandler> logger)
        {
            _healthCenterRepository = healthCenterRepository ?? throw new ArgumentNullException(nameof(healthCenterRepository));
            _healthCenterDxos = healthCenterDxos ?? throw new ArgumentNullException(nameof(healthCenterDxos));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<List<HealthCenterDto>> Handle(GetAllHealthCentersQuery request, CancellationToken cancellationToken)
        {
            var result = await _healthCenterRepository.GetListAsync(e => e.Id != 0);

            if (result != null)
            {
                _logger.LogInformation("Got a request get all insurance companies.");
                List<HealthCenterDto> healthCenterDtos = new List<HealthCenterDto>();
                
                foreach (var item in result)
                {
                    HealthCenterDto healthCenterDto = _healthCenterDxos.MapHealthCenterDto(item);
                    healthCenterDtos.Add(healthCenterDto);
                }
                return healthCenterDtos;
            }

            return null;
        }
    }
}