using System;
using System.Threading;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using OctopusBox.Domain.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace OctopusBox.Service.Subcribers
{
    public class InsuranceCompanyCreatedHandler : INotificationHandler<InsuranceCompanyCreatedEvent>
    {
        private readonly IInsuranceCompanyRepository _insuranceCompanyRepository;
        private readonly ILogger _logger;

        public InsuranceCompanyCreatedHandler(IInsuranceCompanyRepository insuranceCompanyRepository, ILogger<InsuranceCompanyCreatedHandler> logger)
        {
            _insuranceCompanyRepository = insuranceCompanyRepository ?? throw new ArgumentNullException(nameof(insuranceCompanyRepository));
            _logger = logger;
        }

        public async Task Handle(InsuranceCompanyCreatedEvent notification, CancellationToken cancellationToken)
        {
            var insuranceCompany = await _insuranceCompanyRepository.GetAsync(e => e.Id == notification.InsuranceCompanyId);

            if (insuranceCompany == null)
            {
                //TODO: Handle next business logic if insuranceCompany is not found
                _logger.LogWarning("InsuranceCompany is not found by insuranceCompany id from publisher");
            }
            else
            {
                _logger.LogInformation($"InsuranceCompany has found by insuranceCompany id: {notification.InsuranceCompanyId} from publisher");
            }
        }
    }
}