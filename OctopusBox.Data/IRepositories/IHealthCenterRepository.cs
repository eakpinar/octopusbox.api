using System.Threading.Tasks;

namespace OctopusBox.Data.IRepositories
{
    public interface IHealthCenterRepository : IRepository<Domain.Models.HealthCenter>
    {
    //    Task<bool> EmailExistAsync(string email);
    }
}