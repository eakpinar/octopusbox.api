using System.Threading.Tasks;

namespace OctopusBox.Data.IRepositories
{
    public interface IInsuranceCompanyRepository : IRepository<Domain.Models.InsuranceCompany>
    {
    //    Task<bool> EmailExistAsync(string email);
    }
}