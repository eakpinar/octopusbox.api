using Microsoft.EntityFrameworkCore;

namespace OctopusBox.Data
{
    public class OctopusBoxDbContextFactory : DesignTimeDbContextFactory<OctopusBoxDbContext>
    {
        protected override OctopusBoxDbContext CreateNewInstance(DbContextOptions<OctopusBoxDbContext> options)
        {
            return new OctopusBoxDbContext(options);
        }
    }
}