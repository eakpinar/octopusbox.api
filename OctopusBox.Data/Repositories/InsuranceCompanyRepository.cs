using System;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace OctopusBox.Data.Repositories
{
    public class InsuranceCompanyRepository : Repository<Domain.Models.InsuranceCompany>, IInsuranceCompanyRepository
    {
        public InsuranceCompanyRepository(OctopusBoxDbContext dbContext) : base(dbContext)
        {
            
        }
    }
}