using System;
using System.Threading.Tasks;
using OctopusBox.Data.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace OctopusBox.Data.Repositories
{
    public class HealthCenterRepository : Repository<Domain.Models.HealthCenter>, IHealthCenterRepository
    {
        public HealthCenterRepository(OctopusBoxDbContext dbContext) : base(dbContext)
        {
            
        }
    }
}