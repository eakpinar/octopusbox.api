﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using OctopusBox.Domain.Models;

namespace OctopusBox.Data
{
    public partial class OctopusBoxContext : DbContext
    {
        public OctopusBoxContext()
        {
        }

        public OctopusBoxContext(DbContextOptions<OctopusBoxContext> options)
            : base(options)
        {
        }

        /*public virtual DbSet<Domain.Models.Environment> Environment { get; set; }
        public virtual DbSet<Function> Function { get; set; }
        public virtual DbSet<HealthCenter> HealthCenter { get; set; }
        public virtual DbSet<HealthCenterGroup> HealthCenterGroup { get; set; }
        public virtual DbSet<HealthCenterInsuranceCompanyFuncs> HealthCenterInsuranceCompanyFuncs { get; set; }
        public virtual DbSet<HealthCenterInsuranceCompanyUrls> HealthCenterInsuranceCompanyUrls { get; set; }
        public virtual DbSet<InsuranceCompany> InsuranceCompany { get; set; }
        public virtual DbSet<InsuranceCompanyConnection> InsuranceCompanyConnection { get; set; }
        public virtual DbSet<InsuranceCompanyFunction> InsuranceCompanyFunction { get; set; }
        public virtual DbSet<Module> Module { get; set; }
        
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Menus> Menus { get; set; }
        public virtual DbSet<RoleMenuRel> RoleMenuRel { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        */

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Username=postgres;Password=1;Database=OctopusBox;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp")
                .HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Domain.Models.Environment>(entity =>
            {
                entity.ToTable("environment", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Function>(entity =>
            {
                entity.ToTable("function", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.ModuleId).HasColumnName("module_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.Function)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("function_mi_fk");
            });

            modelBuilder.Entity<HealthCenter>(entity =>
            {
                entity.ToTable("health_center", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CgmPassword)
                    .HasColumnName("cgm_password")
                    .HasColumnType("character varying");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.HealthCenterGroupId).HasColumnName("health_center_group_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.HealthCenterGroup)
                    .WithMany(p => p.HealthCenter)
                    .HasForeignKey(d => d.HealthCenterGroupId)
                    .HasConstraintName("health_center_hcgi_fk");
            });

            modelBuilder.Entity<HealthCenterGroup>(entity =>
            {
                entity.ToTable("health_center_group", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<HealthCenterInsuranceCompanyFuncs>(entity =>
            {
                entity.ToTable("health_center_insurance_company_funcs", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.FunctionId).HasColumnName("function_id");

                entity.Property(e => e.HealthCenterId).HasColumnName("health_center_id");

                entity.Property(e => e.InsuranceCompanyId).HasColumnName("insurance_company_id");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.HealthCenterInsuranceCompanyFuncs)
                    .HasForeignKey(d => d.FunctionId)
                    .HasConstraintName("health_center_insurance_company_funcs_fi_fk");

                entity.HasOne(d => d.HealthCenter)
                    .WithMany(p => p.HealthCenterInsuranceCompanyFuncs)
                    .HasForeignKey(d => d.HealthCenterId)
                    .HasConstraintName("health_center_insurance_company_funcs_hci_fk");

                entity.HasOne(d => d.InsuranceCompany)
                    .WithMany(p => p.HealthCenterInsuranceCompanyFuncs)
                    .HasForeignKey(d => d.InsuranceCompanyId)
                    .HasConstraintName("health_center_insurance_company_funcs_ici_fk");
            });

            modelBuilder.Entity<HealthCenterInsuranceCompanyUrls>(entity =>
            {
                entity.ToTable("health_center_insurance_company_urls", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.HealthCenterId).HasColumnName("health_center_id");

                entity.Property(e => e.HealthCenterName)
                    .HasColumnName("health_center_name")
                    .HasColumnType("character varying");

                entity.Property(e => e.HealthCenterPassword)
                    .HasColumnName("health_center_password")
                    .HasColumnType("character varying");

                entity.Property(e => e.InsuranceCompanyConnectionId).HasColumnName("insurance_company_connection_id");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.HealthCenter)
                    .WithMany(p => p.HealthCenterInsuranceCompanyUrls)
                    .HasForeignKey(d => d.HealthCenterId)
                    .HasConstraintName("health_center_insurance_company_urls_hci_fk");

                entity.HasOne(d => d.InsuranceCompanyConnection)
                    .WithMany(p => p.HealthCenterInsuranceCompanyUrls)
                    .HasForeignKey(d => d.InsuranceCompanyConnectionId)
                    .HasConstraintName("health_center_insurance_company_urls_icci_fk");
            });

            modelBuilder.Entity<InsuranceCompany>(entity =>
            {
                entity.ToTable("insurance_company", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<InsuranceCompanyConnection>(entity =>
            {
                entity.ToTable("insurance_company_connection", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ConnectionUrl)
                    .HasColumnName("connection_url")
                    .HasColumnType("character varying");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.EnvironmentId).HasColumnName("environment_id");

                entity.Property(e => e.InsuranceCompanyId).HasColumnName("insurance_company_id");

                entity.Property(e => e.ModuleId).HasColumnName("module_id");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.Environment)
                    .WithMany(p => p.InsuranceCompanyConnection)
                    .HasForeignKey(d => d.EnvironmentId)
                    .HasConstraintName("insurance_company_connection_ei_fk");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.InsuranceCompanyConnection)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("insurance_company_connection_mi_fk");
            });

            modelBuilder.Entity<InsuranceCompanyFunction>(entity =>
            {
                entity.ToTable("insurance_company_function", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.FunctionId).HasColumnName("function_id");

                entity.Property(e => e.InsuranceCompanyId).HasColumnName("insurance_company_id");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.InsuranceCompanyFunction)
                    .HasForeignKey(d => d.FunctionId)
                    .HasConstraintName("insurance_company_function_fi_fk");

                entity.HasOne(d => d.InsuranceCompany)
                    .WithMany(p => p.InsuranceCompanyFunction)
                    .HasForeignKey(d => d.InsuranceCompanyId)
                    .HasConstraintName("insurance_company_function_ici_fk");
            });

            modelBuilder.Entity<Menus>(entity =>
            {
                entity.ToTable("menus", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.IconString)
                    .HasColumnName("icon_string")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link)
                    .HasColumnName("link")
                    .HasColumnType("character varying");

                entity.Property(e => e.MenuName)
                    .IsRequired()
                    .HasColumnName("menu_name")
                    .HasColumnType("character varying");

                entity.Property(e => e.ParentId).HasColumnName("parent_id");

                entity.Property(e => e.ShowInMenu)
                    .HasColumnName("show_in_menu")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'1'::\"bit\"");

                entity.Property(e => e.SortIndex).HasColumnName("sort_index");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.ToTable("module", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<RoleMenuRel>(entity =>
            {
                entity.ToTable("role_menu_rel", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.MenuId).HasColumnName("menu_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.RoleMenuRel)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("role_menu_rel_menus_id_fk");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleMenuRel)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("role_menu_rel_roles_id_fk");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.ToTable("roles", "OctopusBox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserName)
                    .HasName("users_pk");

                entity.ToTable("users", "OctopusBox");

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasColumnType("character varying")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("date");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("character varying");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("date");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("users_ri_fk");
            });
        }
    }
}
