using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using OctopusBox.Domain.Commands;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using OctopusBox.Domain.Queries.HealthCenter;

namespace OctopusBox.API.Controllers
{
    public class HealthCenterController : ApiControllerBase
    {
        public HealthCenterController(IMediator mediator) : base(mediator)
        {
        }

        /// <summary>
        /// Get health center by id
        /// </summary>
        /// <param name="id">Id of health center</param>
        /// <returns>Insurance Company information</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(HealthCenterDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<HealthCenterDto>> GetHealthCenterAsync(long id)
        {
            return Single(await QueryAsync(new GetHealthCenterQuery(id)));
        }

        /// <summary>
        /// Get all health center
        /// </summary>
        /// <returns>Health Centers information</returns>
        [HttpGet()]
        [ProducesResponseType(typeof(List<HealthCenterDto>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<HealthCenterDto>>> GetAllHealthCentersAsync()
        {
            return Single(await QueryAsync(new GetAllHealthCentersQuery()));
        }

        /// <summary>
        /// Create new health center
        /// </summary>
        /// <param name="command">Info of health center</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> CreateHealthCenterAsync([FromBody] CreateHealthCenterCommand command)
        {
            return Ok(await CommandAsync(command));
        }

        /// <summary>
        /// Delete health center
        /// </summary>
        /// <param name="command">Info of health center</param>
        /// <returns></returns>
        [HttpDelete()]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> DeleteHealthCenterAsync([FromBody] DeleteHealthCenterCommand command)
        {
            return Ok(await CommandAsync(command));
        }
        
        /// <summary>
        /// Update health center
        /// </summary>
        /// <param name="command">Info of health center</param>
        /// <returns></returns>
        [HttpPut()]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> UpdateHealthCenterAsync([FromBody] UpdateHealthCenterCommand command)
        {
            return Ok(await CommandAsync(command));
        }
    }
}