using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using OctopusBox.Domain.Commands;
using OctopusBox.Domain.Dtos;
using OctopusBox.Domain.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using OctopusBox.Domain.Queries.InsuranceCompany;

namespace OctopusBox.API.Controllers
{

    public class InsuranceCompanyController : ApiControllerBase

    {
        public InsuranceCompanyController(IMediator mediator) : base(mediator)
        {
        }

        /// <summary>
        /// Get insurance company by id
        /// </summary>
        /// <param name="id">Id of insurance company</param>
        /// <returns>Insurance Company information</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(InsuranceCompanyDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<InsuranceCompanyDto>> GetInsuranceCompanyAsync(long id)
        {
            return Single(await QueryAsync(new GetInsuranceCompanyQuery(id)));
        }

        /// <summary>
        /// Get all insurance company
        /// </summary>
        /// <returns>Insurance Companies information</returns>
        [HttpGet()]
        [ProducesResponseType(typeof(List<InsuranceCompanyDto>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<InsuranceCompanyDto>>> GetAllInsuranceCompaniesAsync()
        {
            
            return Single(await QueryAsync(new GetAllInsuranceCompaniesQuery()));
        }

        /// <summary>
        /// Create new insurance company
        /// </summary>
        /// <param name="command">Info of insurance company</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> CreateInsuranceCompanyAsync([FromBody] CreateInsuranceCompanyCommand command)
        {
            return Ok(await CommandAsync(command));
        }

        /// <summary>
        /// Delete insurance company
        /// </summary>
        /// <param name="command">Info of insurance company</param>
        /// <returns></returns>
        [HttpDelete()]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> DeleteInsuranceCompanyAsync([FromBody] DeleteInsuranceCompanyCommand command)
        {
            return Ok(await CommandAsync(command));
        }


        /// <summary>
        /// Update insurance company
        /// </summary>
        /// <param name="command">Info of insurance company</param>
        /// <returns></returns>
        [HttpPut()]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> UpdateInsuranceCompanyAsync([FromBody] UpdateInsuranceCompanyCommand command)
        {
            return Ok(await CommandAsync(command));
        }

        
        
    }
}