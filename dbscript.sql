CREATE TABLE "OctopusBox".module
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT module_pk PRIMARY KEY (id)
);
ALTER TABLE "OctopusBox".module
    OWNER to postgres;


CREATE TABLE "OctopusBox".function
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    module_id bigint,
    CONSTRAINT function_pk PRIMARY KEY (id),
    CONSTRAINT function_mi_fk FOREIGN KEY (module_id)
        REFERENCES "OctopusBox".module (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".function
    OWNER to postgres;
    

CREATE TABLE "OctopusBox".roles
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT roles_pk PRIMARY KEY (id)
);
ALTER TABLE "OctopusBox".roles
    OWNER to postgres;


CREATE TABLE "OctopusBox".users
(
    id bigint NOT NULL,
    user_name character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default",
    role_id bigint,
    CONSTRAINT users_pk PRIMARY KEY (user_name),
    CONSTRAINT users_ri_fk FOREIGN KEY (role_id)
        REFERENCES "OctopusBox".roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".users
    OWNER to postgres;
    

CREATE TABLE "OctopusBox".health_center_group
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT health_center_group_pk PRIMARY KEY (id)
);
ALTER TABLE "OctopusBox".health_center_group
    OWNER to postgres;


CREATE TABLE "OctopusBox".health_center
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    cgm_password character varying COLLATE pg_catalog."default",
    health_center_group_id bigint,
    CONSTRAINT health_center_pk PRIMARY KEY (id),
    CONSTRAINT health_center_hcgi_fk FOREIGN KEY (health_center_group_id)
        REFERENCES "OctopusBox".health_center_group (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".health_center
    OWNER to postgres;


CREATE TABLE "OctopusBox".insurance_company
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT insurance_company_pk PRIMARY KEY (id)
);
ALTER TABLE "OctopusBox".insurance_company
    OWNER to postgres;


CREATE TABLE "OctopusBox".environment
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT environment_pk PRIMARY KEY (id)
);
ALTER TABLE "OctopusBox".environment
    OWNER to postgres;


CREATE TABLE "OctopusBox".insurance_company_connection
(
    id bigint NOT NULL,
    insurance_company_id bigint,
    connection_url character varying COLLATE pg_catalog."default",
    module_id bigint,
    environment_id bigint,
    CONSTRAINT insurance_company_connection_pk PRIMARY KEY (id),
    CONSTRAINT insurance_company_connection_mi_fk FOREIGN KEY (module_id)
        REFERENCES "OctopusBox".module (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT insurance_company_connection_ei_fk FOREIGN KEY (environment_id)
        REFERENCES "OctopusBox".environment (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".insurance_company_connection
    OWNER to postgres;


CREATE TABLE "OctopusBox".insurance_company_function
(
    id bigint NOT NULL,
    insurance_company_id bigint,
    function_id bigint,
    CONSTRAINT insurance_company_function_pk PRIMARY KEY (id),
    CONSTRAINT insurance_company_function_fi_fk FOREIGN KEY (function_id)
        REFERENCES "OctopusBox".function (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT insurance_company_function_ici_fk FOREIGN KEY (insurance_company_id)
        REFERENCES "OctopusBox".insurance_company (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".insurance_company_function
    OWNER to postgres;



CREATE TABLE "OctopusBox".health_center_insurance_company_funcs
(
    id bigint NOT NULL,
    health_center_id bigint,
    insurance_company_id bigint,
    function_id bigint,
    CONSTRAINT health_center_insurance_company_funcs_pk PRIMARY KEY (id),
    CONSTRAINT health_center_insurance_company_funcs_fi_fk FOREIGN KEY (function_id)
        REFERENCES "OctopusBox".function (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT health_center_insurance_company_funcs_ici_fk FOREIGN KEY (insurance_company_id)
        REFERENCES "OctopusBox".insurance_company (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT health_center_insurance_company_funcs_hci_fk FOREIGN KEY (health_center_id)
        REFERENCES "OctopusBox".health_center (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".health_center_insurance_company_funcs
    OWNER to postgres;



CREATE TABLE "OctopusBox".health_center_insurance_company_urls
(
    id bigint NOT NULL,
    health_center_id bigint,
    insurance_company_connection_id bigint,
    health_center_name character varying COLLATE pg_catalog."default",
    health_center_password character varying COLLATE pg_catalog."default",
    CONSTRAINT health_center_insurance_company_urls_pk PRIMARY KEY (id),
    CONSTRAINT health_center_insurance_company_urls_icci_fk FOREIGN KEY (insurance_company_connection_id)
        REFERENCES "OctopusBox".insurance_company_connection (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT health_center_insurance_company_urls_hci_fk FOREIGN KEY (health_center_id)
        REFERENCES "OctopusBox".health_center (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE "OctopusBox".health_center_insurance_company_urls
    OWNER to postgres;


ALTER TABLE "OctopusBox".module ADD COLUMN created date;
ALTER TABLE "OctopusBox".module ADD COLUMN updated date;
ALTER TABLE "OctopusBox".function ADD COLUMN created date;
ALTER TABLE "OctopusBox".function ADD COLUMN updated date;
ALTER TABLE "OctopusBox".roles ADD COLUMN created date;
ALTER TABLE "OctopusBox".roles ADD COLUMN updated date;
ALTER TABLE "OctopusBox".users ADD COLUMN created date;
ALTER TABLE "OctopusBox".users ADD COLUMN updated date;
ALTER TABLE "OctopusBox".health_center_group ADD COLUMN created date;
ALTER TABLE "OctopusBox".health_center_group ADD COLUMN updated date;
ALTER TABLE "OctopusBox".health_center ADD COLUMN created date;
ALTER TABLE "OctopusBox".health_center ADD COLUMN updated date;
ALTER TABLE "OctopusBox".insurance_company ADD COLUMN created date;
ALTER TABLE "OctopusBox".insurance_company ADD COLUMN updated date;
ALTER TABLE "OctopusBox".environment ADD COLUMN created date;
ALTER TABLE "OctopusBox".environment ADD COLUMN updated date;
ALTER TABLE "OctopusBox".insurance_company_connection ADD COLUMN created date;
ALTER TABLE "OctopusBox".insurance_company_connection ADD COLUMN updated date;
ALTER TABLE "OctopusBox".insurance_company_function ADD COLUMN created date;
ALTER TABLE "OctopusBox".insurance_company_function ADD COLUMN updated date;
ALTER TABLE "OctopusBox".health_center_insurance_company_funcs ADD COLUMN created date;
ALTER TABLE "OctopusBox".health_center_insurance_company_funcs ADD COLUMN updated date;
ALTER TABLE "OctopusBox".health_center_insurance_company_urls ADD COLUMN created date;
ALTER TABLE "OctopusBox".health_center_insurance_company_urls ADD COLUMN updated date;


